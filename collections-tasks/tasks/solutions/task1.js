// Usable properties
export default function (data) {
  // your code starts here
  return data.map((obj) =>
    obj.usable.reduce((acc, curr) => {
      acc[curr] = obj[curr];
      return acc;
    }, {}))

  // your code ends here
}
