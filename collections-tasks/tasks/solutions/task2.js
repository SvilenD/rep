// People with cities and population
/**
 * 
 * @param {{cities: Array<{ id: number, name: string }>, people: Array<{ name: string, city: number }>}} data 
 */
export default function (data) {
  return data.people.reduce((acc, curr) => {
    const person = {};
    const cityName = data.cities.find(value => value.id === curr.city).name; 
    const population = data.people.filter(value => value.city === curr.city).length;
    person.name = curr.name;
    person.city = {name:cityName, population};
    acc.push(person);

    return acc;
  }, [])
}
